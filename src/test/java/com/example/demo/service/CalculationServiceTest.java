package com.example.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculationServiceTest {
    @Autowired
    private CalculationService service;

    @Test
    public void checkSumAsParams() {
        BigDecimal result = service.calculate("sum", BigDecimal.ONE, BigDecimal.TEN);
        assertThat(result, comparesEqualTo(new BigDecimal("11")));
    }

    @Test
    public void checkSubtractAsParams() {
        BigDecimal result = service.calculate("sub", BigDecimal.ONE, BigDecimal.TEN);
        assertThat(result, comparesEqualTo(new BigDecimal("-9")));
    }

    @Test
    public void checkMultiplyAsParams() {
        BigDecimal result = service.calculate("mult", new BigDecimal("2"), BigDecimal.TEN);
        assertThat(result, comparesEqualTo(new BigDecimal("20")));
    }

    @Test
    public void checkDivisionNoRounding() {
        BigDecimal result = service.calculate("div", new BigDecimal("20"), BigDecimal.TEN);
        assertThat(result, comparesEqualTo(new BigDecimal("2")));
    }

    @Test
    public void checkDivisionWithRounding() {
        BigDecimal result = service.calculate("div", new BigDecimal("5"), new BigDecimal("3"));
        assertThat(result, comparesEqualTo(new BigDecimal("1.666666666666667")));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void checkInvalidOperation() {
        service.calculate("sadfsd", new BigDecimal("5"), new BigDecimal("3"));
        fail();
    }

    @Test(expected = ArithmeticException.class)
    public void checkDivisionByZero() {
        service.calculate("div", new BigDecimal("5"), new BigDecimal("0"));
        fail();
    }
}