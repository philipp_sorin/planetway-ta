package com.example.demo.web;

import com.example.demo.domain.dto.EmployeeDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OperationControllerTests {

    @Autowired
    private OperationController controller;

    @Test
    public void contextLoads() {
    }

    @Test
    public void checkHelloController() {
        String result = controller.sayHello();
        assertThat(result, is("Hello world!"));
    }

    @Test
    public void checkRqParamsCorrectlyPassed() {
        BigDecimal result = controller.calculateUsingParams("div", new BigDecimal("20"), BigDecimal.TEN);
        assertThat(result, comparesEqualTo(new BigDecimal("2")));
    }

    @Test
    public void checkDtoParamsCorrectlyPassed() {
        OperationController.CalculateRequestDto dto = new OperationController.CalculateRequestDto();
        dto.setOp("div");
        dto.setNum1(new BigDecimal("20"));
        dto.setNum2(new BigDecimal("2"));

        BigDecimal result = controller.calculateUsingDto(dto);
        assertThat(result, comparesEqualTo(new BigDecimal("10")));
    }

    @Test
    public void getAllEmployees() {
        OperationController.EmployeesResponse allEmpl = controller.getEmployees();
        List<EmployeeDto> employees = allEmpl.getEmployees();
        assertThat(employees, hasSize(2));

        List<EmployeeDto> supervisors = allEmpl.getSupervisors();
        assertThat(supervisors, hasSize(2));

        assertThat(extract(employees, EmployeeDto::getName), containsInAnyOrder("Olexii", "Znak"));
        assertThat(extract(extract(employees, EmployeeDto::getSupervisor), EmployeeDto::getName), containsInAnyOrder("Sergio", "Sergio"));

        assertThat(extract(supervisors, EmployeeDto::getName), containsInAnyOrder("Antonio", "Sergio"));

        EmployeeDto serg = findEmp(supervisors, "Sergio");
        assertThat(serg.getSupervisor().getName(), is("Antonio"));

        EmployeeDto ant = findEmp(supervisors, "Antonio");
        assertThat(ant.getSupervisor(), is(nullValue()));
    }

    private EmployeeDto findEmp(List<EmployeeDto> supervisors, String name) {
        return supervisors.stream().filter(it -> it.getName().equals(name)).findAny().get();
    }


    private <O, T> List<T> extract(List<O> employees, Function<O, T> field) {
        return employees.stream().map(field).collect(Collectors.toList());
    }
}
