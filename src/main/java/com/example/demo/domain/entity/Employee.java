package com.example.demo.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Employee implements Serializable {

    @Id
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private boolean supervisor;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "supervisor_id")
    private Employee supervisorEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public void setSupervisor(boolean supervisor) {
        this.supervisor = supervisor;
    }

    public Employee getSupervisorEntity() {
        return supervisorEntity;
    }

    public void setSupervisorEntity(Employee supervisorEntity) {
        this.supervisorEntity = supervisorEntity;
    }
}