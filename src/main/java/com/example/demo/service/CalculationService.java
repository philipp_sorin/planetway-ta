package com.example.demo.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.function.BiFunction;

@Service
public class CalculationService {

    private enum Ops {
        SUM(BigDecimal::add),
        SUB(BigDecimal::subtract),
        DIV((num1, num2) -> num1.divide(num2, 15, BigDecimal.ROUND_HALF_EVEN)),
        MULT(BigDecimal::multiply);

        private String alias;
        private BiFunction<BigDecimal, BigDecimal, BigDecimal> operation;

        Ops(BiFunction<BigDecimal, BigDecimal, BigDecimal> operation) {
            alias = name().toLowerCase();
            this.operation = operation;
        }
    }


    public BigDecimal calculate(String op, BigDecimal num1, BigDecimal num2) {
        for (Ops value : Ops.values()) {
            if (Objects.equals(value.alias, op)) {
                return value.operation.apply(num1, num2);
            }
        }
        throw new UnsupportedOperationException(op);
    }
}
