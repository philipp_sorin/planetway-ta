package com.example.demo.service;

import com.example.demo.domain.dto.EmployeeDto;
import com.example.demo.domain.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Transactional(readOnly = true)
    public List<EmployeeDto> getEmployees() {
        List<Employee> employees = repository.getBySupervisor(false);
        return convertToDtos(employees);
    }

    @Transactional(readOnly = true)
    public List<EmployeeDto> getSupervisors() {
        List<Employee> supervisors = repository.getBySupervisor(true);
        return convertToDtos(supervisors);
    }

    private List<EmployeeDto> convertToDtos(List<Employee> entities) {
        return entities.stream().map(this::toDto).collect(toList());
    }

    private EmployeeDto toDto(Employee employee) {
        EmployeeDto dto = new EmployeeDto();
        dto.setId(employee.getId());
        dto.setName(employee.getName());
        Employee supervisor = employee.getSupervisorEntity();
        if (supervisor != null) {
            dto.setSupervisor(toDto(supervisor));
        }
        return dto;
    }
}
