package com.example.demo.web;

import com.example.demo.domain.dto.EmployeeDto;
import com.example.demo.service.CalculationService;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@RestController
public class OperationController {

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private EmployeeService employeeService;


    @GetMapping(value = "/hello", produces = {MediaType.TEXT_PLAIN_VALUE})
    public String sayHello() {
        return "Hello world!";
    }

    @GetMapping(value = "/calculate", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public BigDecimal calculateUsingParams(@RequestParam String op, @RequestParam BigDecimal num1, @RequestParam BigDecimal num2) {
        return calculationService.calculate(op, num1, num2);
    }

    @PostMapping(value = "/calculate", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public BigDecimal calculateUsingDto(@Valid @RequestBody CalculateRequestDto rq) {
        return calculationService.calculate(rq.op, rq.num1, rq.num2);
    }

    public static class CalculateRequestDto {
        @NotNull
        private String op;
        @NotNull
        private BigDecimal num1;
        @NotNull
        private BigDecimal num2;

        public void setOp(String op) {
            this.op = op;
        }

        public void setNum1(BigDecimal num1) {
            this.num1 = num1;
        }

        public void setNum2(BigDecimal num2) {
            this.num2 = num2;
        }
    }


    public static class EmployeesResponse {
        private List<EmployeeDto> supervisors;
        private List<EmployeeDto> employees;

        public EmployeesResponse(List<EmployeeDto> supervisors, List<EmployeeDto> employees) {
            this.supervisors = supervisors;
            this.employees = employees;
        }

        public List<EmployeeDto> getSupervisors() {
            return supervisors;
        }

        public List<EmployeeDto> getEmployees() {
            return employees;
        }
    }

    @GetMapping(value = "/employees", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public EmployeesResponse getEmployees() {
        return new EmployeesResponse(employeeService.getSupervisors(), employeeService.getEmployees());
    }


}
