package com.example.client.domain;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CalculateRequest {

    @NotNull
    private String op;
    @NotNull
    private BigDecimal num1;
    @NotNull
    private BigDecimal num2;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public BigDecimal getNum1() {
        return num1;
    }

    public void setNum1(BigDecimal num1) {
        this.num1 = num1;
    }

    public BigDecimal getNum2() {
        return num2;
    }

    public void setNum2(BigDecimal num2) {
        this.num2 = num2;
    }
}
