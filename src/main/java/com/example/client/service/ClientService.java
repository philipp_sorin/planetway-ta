package com.example.client.service;

import com.example.client.communication.Client;
import com.example.client.domain.CalculateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ClientService {

    @Autowired
    private Client client;

    public BigDecimal calculate(BigDecimal num1, BigDecimal num2, String op) {
        CalculateRequest requestDto = new CalculateRequest();
        requestDto.setNum1(num1);
        requestDto.setNum2(num2);
        requestDto.setOp(op);
        return client.getResponse(requestDto);
    }
}
