package com.example.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PostBootService {

    @Autowired
    private ClientService clientService;

    @EventListener(ApplicationReadyEvent.class)
    public void onBoot() {
        System.err.println(clientService.calculate(BigDecimal.ONE, BigDecimal.TEN, "sum"));
    }
}
