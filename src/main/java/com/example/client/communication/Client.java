package com.example.client.communication;

import com.example.client.domain.CalculateRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;

@FeignClient(name = "client", url = "http://127.0.0.1:9090")
public interface Client {

    @RequestMapping(method = RequestMethod.POST, value = "/calculate")
    BigDecimal getResponse(@RequestBody CalculateRequest request);
}
