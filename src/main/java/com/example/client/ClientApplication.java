package com.example.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.util.HashMap;

@EnableFeignClients
@SpringBootApplication
public class ClientApplication {
	public static void main(String[] args) {
		HashMap<String, Object> props = new HashMap<>();
		props.put("server.port", 9999);

		new SpringApplicationBuilder()
				.sources(ClientApplication.class)
				.properties(props)
				.run(args);
	}
}
